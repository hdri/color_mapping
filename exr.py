import array
import OpenEXR
import Imath
import cv2
import os
import numpy as np

FLOAT = Imath.PixelType(Imath.PixelType.FLOAT)

def open(fname):
    name, extension = os.path.splitext(fname)
    #print 'extension: ', extension
    if extension == '.exr': # TODO: also for other formats?
        img = cv2.imread(fname, cv2.IMREAD_UNCHANGED)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    else:
        img = cv2.imread(fname)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        
        #height = len(img)
        #width = len(img[0])
        #channels = len(img[0][0])

        #img = cv2.cvtColor(img, cv2.COLOR_BGR2XYZ)
        #print img.dtype

        if img.dtype == 'uint8':
            #print('uint8 to float')
            #img = img.astype(np.float128) / 255
            img = np.float32(img) / 255.
            #print(img.dtype)

            def sRGB_to_linear(val, gamma=2.4):
                if val <= 0.04045:
                    return val / 12.92
                return pow((val + 0.055) / (1 + 0.055), gamma)

            sRGB_to_linear_vectorized = np.vectorize(sRGB_to_linear)
            img = sRGB_to_linear_vectorized(img) # note: this converts the matrix to float64
            #print(img.dtype)
            img = np.float32(img)
            #print(img.dtype)

            #print('uint8 to float done')
            pass

    return img

#def get_header(fname):
#    return OpenEXR.InputFile(fname).header()

def get_height(img):
    return len(img)

def get_width(img):
    return len(img[0])

def get_channels(img):
    return len(img[0][0])


def to_grayscale(img):
    return cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)


def create_output_file(fname, width, height):
    header = OpenEXR.Header(width, height)
    channel_type = Imath.Channel(FLOAT)
    header['channels'] = dict([(c, channel_type) for c in "RGB"])
    return OpenEXR.OutputFile(fname, header)

# Open two images and check they have the same resolution
def open_two_images(first_fname, second_fname):
    for f in [first_fname, second_fname]:
        if not os.path.isfile(f):
            raise Exception("File {} does not exist".format(f))

    # Open the images
    s = open(first_fname)
    t = open(second_fname)
    # Check that the resolutions are the same
    assert (get_width(s) == get_width(t))
    assert (get_height(s) == get_height(t))
    assert (get_channels(s) == get_channels(t))

    return (s, t)
