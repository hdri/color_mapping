import random

import sys

import exr
import utils
import cv2

random.seed(123)

import numpy as np
import pandas as pd
import scipy.optimize
import matplotlib.pyplot as pp

def rmse(a, b):
    assert(len(a) == len(b))
    sum_squared_error = 0
    count = 0
    for i in range(len(a)):
        diff = a[i] - b[i]
        sum_squared_error += diff * diff
        count += 1
    mse = float(sum_squared_error) / count
    return np.sqrt(mse)

class Linearizer:
    x = []  # ground truth
    y = []  # reconstruction

    # only x in 0..1 range:
    x_01 = []
    y_01 = []

    #def __init__(self):

    def load_csv(self, fname):
        mapping = pd.read_csv(fname)

        self.x = mapping['source_Y'].values
        self.y = mapping['target_Y'].values

        assert(len(self.x) == len(self.y))

        for i in range(len(self.x)):
            if self.x[i] < 1:
                self.x_01.append(self.x[i])
                self.y_01.append(self.y[i])

    def add_pair(self, x, y):
        self.x.append(x)
        self.y.append(y)
        if x < 1:
            self.x_01.append(x)
            self.y_01.append(y)

    def fit(self, func, everywhere = False): # fit the 0..1 range (find the dependency of x on y ... we want to find the function to recover the original x from y)
        assert (len(self.x_01) > 0)
        self.func = func

        if everywhere:
            x = self.x
            y = self.y
        else:
            x = self.x_01
            y = self.y_01

        self.popt, self.pcov = scipy.optimize.curve_fit(func, y, x)
        #perr = np.sqrt(np.diag(self.pcov))
        #return perr

    def get_linearization_rmse(self):
        return rmse(self.x, self.linearize(self.y))

    def linearize(self, input):
        return self.func(input, *self.popt)

    def plot(self):
        # luminance mapping data
        pp.plot(self.x, self.y, '+')

        # y = x
        x_values = np.linspace(0, 5, 1000)
        pp.plot(x_values, x_values, 'k--')

        # fitted inverse (y -> x) function (drawn with swapped axes to show how it matches the input mapping)
        pp.plot(self.func(x_values, *self.popt), x_values, 'y-')

        # generate luminance mapping scatterplot to compare the linearized y with ground truth - it should be on the "y = x" line
        linearized_y = self.linearize(self.y)
        pp.plot(self.x, linearized_y, 'gx')

        pp.show()

# fitting functions
def mul(x, a):
    return a * x

def linear(x, a, b):
    return a * x + b

def polynomial2(x, a, b, c):
    return a + b * x + c * x * x

def polynomial3(x, a, b, c, d):
    return a + b * x + c * x * x + d * x * x * x

def polynomial4(x, a, b, c, d, e):
    return a + b * x + c * x * x + d * x * x * x + e * x * x * x * x

def gamma_correct(x, gamma):
    return np.power(x, gamma)

def exp(x, a, b, base):
    return a + np.power(base, x) * b

def polynomial_gamma(x, a, b, c, gamma):
    #gx = gamma_correct(x, gamma)
    #return a + b * gx + c * gx * gx
    return polynomial2(gamma_correct(x, gamma), a, b, c)

def linear_gamma(x, a, b, gamma):
    return linear(gamma_correct(x, gamma), a, b)

def gamma_linear(x, a, b, gamma):
    return gamma_correct(linear(x, a, b), gamma)

#def mulshiftexp(x, a, b, c):
#    return a * np.exp(-b * x) + c

# TODO: identity function (without parameters) as a baseline?


def testfit():
    # for REC in /777/repos/Diplomova\ prace/pipeline/temp/*_expandnet.exr; do GT=$(echo "$REC" | sed 's/.jpg_expandnet.exr//'); echo $GT; python3 get_color_mapping.py --luminance "$GT" "$REC" > mapping_"$(basename "$GT")"_"$(basename "$REC")".csv; done

    files = '''
04-16_Sky.exr
04-17_Cloudy.exr
04-18_Cloudy.exr
04-23_Day_A.exr
04-23_Day_B.exr
04-23_Day_C.exr
04-23_Day_D.exr
05-08_Day_A.exr
05-08_Day_B.exr
05-16_Day_A.exr
05-16_Day_B.exr
05-16_Day_C.exr
05-16_Day_D.exr
05-16_Day_E.exr
05-16_Day_F.exr
05-18_Day_A.exr
05-18_Day_B.exr
05-18_Day_C.exr
05-18_Day_E.exr
05-18_Day_F.exr
05-18_Day_G.exr
05-28_Day_A.exr
05-28_Day_B.exr
05-28_Day_C.exr
05-28_Day_D.exr
06-07_Day_A.exr
06-07_Day_B.exr
06-07_Day_C.exr
06-07_Day_D.exr
06-07_Day_E.exr
06-07_Day_F.exr
06-07_Day_G.exr
06-07_Day_H.exr
10-Shiodome_Stairs_3k.exr
14-Hamarikyu_Bridge_B_3k.exr
20_Subway_Lights_3k.exr
Alexs_Apt_2k.exr
Arches_E_PineTree_3k.exr
Barce_Rooftop_C_3k.exr
BasketballCourt_3k.exr
Brooklyn_Bridge_Planks_2k.exr
Bryant_Park_2k.exr
Chelsea_Stairs_3k.exr
Circus_Backstage_3k.exr
Ditch-River_2k.exr
Etnies_Park_Center_3k.exr
Factory_Catwalk_2k.exr
Footprint_Court_2k.exr
Frozen_Waterfall_Ref.exr
GCanyon_C_YumaPoint_3k.exr
GCanyon_C_YumaPoint_3k_centered.exr
GravelPlaza_REF.exr
HWSign3-Fence_2k.exr
Ice_Lake_Ref.exr
LA_Downtown_Afternoon_Fishing_3k.exr
LA_Downtown_Helipad_GoldenHour_3k.exr
Lobby-Center_2k.exr
Malibu_Overlook_3k.exr
Milkyway_small.exr
MonValley_A_LookoutPoint_2k.exr
MonValley_G_DirtRoad_3k.exr
Mono_Lake_B_Ref.exr
Mono_Lake_C_Ref.exr
NarrowPath_3k.exr
Newport_Loft_Ref.exr
PaperMill_A_3k.exr
PaperMill_E_3k.exr
Playa_Sunrise.exr
QueenMary_Chimney_Ref.exr
Ridgecrest_Road_Ref.exr
Road_to_MonumentValley_Ref.exr
Serpentine_Valley_3k.exr
Sierra_Madre_B_Ref.exr
Siggraph2007_UpperFloor_REF.exr
Stadium_Center_3k.exr
rad06_Ref.exr
test8_Ref.exr
'''

    #files = 'test8_Ref.exr'
    #files = '05-16_Day_A.exr'

    # TODO: why does the fitting become slower when the script runs for a long time? (also the accuracy seems to go down...)

    files = files.splitlines()
    random.shuffle(files)

    #files = files[0:5] # first five

    for file in files:
        if file == '':
            continue

        file = 'mapping_{}_{}.jpg_expandnet.exr.csv'.format(file, file)

        #print(file + '\t', end='')

        l = Linearizer()

        #l.load_csv('mapping_06-07_Day_F.exr_06-07_Day_F.exr.jpg_drtmo.exr.csv')
        #l.load_csv('mapping_Lobby-Center_2k.exr_Lobby-Center_2k.exr.jpg_drtmo.exr.csv')
        l.load_csv(file)

        # choose which functions to test
        funcs = [mul, linear, polynomial2, polynomial3, polynomial4, gamma_correct, exp, polynomial_gamma, linear_gamma]
        #funcs = [linear, polynomial2, gamma_correct, polynomial_gamma, linear_gamma, gamma_linear]
        #funcs = [linear, gamma_correct, linear_gamma]
        #funcs = [linear]
        #funcs = [mul]
        best_error = np.Infinity
        best_func = None
        best_popt = None

        for f in funcs:
            try:
                #l.fit(f, True)
                l.fit(f, False)
                error = l.get_linearization_rmse()

                print('{}\t{}\t{}\t{}'.format(file, error, f.__name__, l.popt)) # put the printed results into a spreadsheet and compute the average RMSE for each straightening function to determine which is the best

                if error < best_error:
                    best_func = f
                    best_popt = l.popt
                    best_error = error

                # visualise the fitting and the straightening result (comment it for non-interactive batch-fitting)
                l.plot()
            except:
                pass

        #print('{}\t{}\t{}'.format(best_error, best_func.__name__, best_popt))

        #l.fit(polynomial_gamma, True)
        #l.plot()
        #l.fit(polynomial_gamma, False)
        #print(l.pcov)
        #l.fit(polynomial2)
        #l.fit(exp)

        #l.plot()

def fix_reconstruction(LDR_fname, reconstruction_fname, fixed_out_fname):
    # Open the images
    print('Opening images')
    (s, t) = exr.open_two_images(LDR_fname, reconstruction_fname)

    #get_brightness = utils.get_brightness_max
    get_brightness = utils.get_brightness_sRGB # luminance

    l = Linearizer()
    print('Loading image data')
    for row in range(exr.get_height(s)):
        s_row = s[row]
        t_row = t[row]
        for column in range(exr.get_width(s)):
            s_pixel = s_row[column]
            t_pixel = t_row[column]

            x = get_brightness(s_pixel[0], s_pixel[1], s_pixel[2]) # LDR value ("ground truth")
            y = get_brightness(t_pixel[0], t_pixel[1], t_pixel[2])

            l.add_pair(x, y)

    print('Fitting')
    l.fit(mul)

    print('Linearizing')
    linearized_t = l.linearize(t)

    print('Saving linearized result')
    #exr.create_output_file(fixed_out_fname, exr.get_width(s), exr.get_height(s))
    cv2.imwrite(fixed_out_fname, cv2.cvtColor(np.float32(linearized_t), cv2.COLOR_RGB2BGR))


#testfit() # uncomment to run the evaluation "how well does each function straighten the luminance mapping curve"

# TODO: check parameter count
fix_reconstruction(sys.argv[1], sys.argv[2], sys.argv[3])
