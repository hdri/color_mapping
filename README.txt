# color mapping
A set of shell and Python scripts for comparing images, computing error metrics and various statistics and generating luminance/color mapping graphs.