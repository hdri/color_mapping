import sys
import exr
import utils
import math

from skimage.measure import compare_ssim as ssim

class Counter:
    pixel_count = 0
    sqr_difference_total = 0  # sum of squares

    def process_pixel(self, sR, sG, sB, tR, tG, tB):
        self.pixel_count = self.pixel_count + 1

        sY = get_brightness(sR, sG, sB)
        tY = get_brightness(tR, tG, tB)
        diff = tY - sY
        self.sqr_difference_total += diff * diff

    def get_average_difference(self): # "root mean square"
        return math.sqrt(self.sqr_difference_total / self.pixel_count)

# TODO: better parameter validation

# Compute RMSE difference of two images

# Open the images
(s, t) = exr.open_two_images(sys.argv[1], sys.argv[2])

#get_brightness = utils.get_brightness_max
get_brightness = utils.get_brightness_sRGB # luminance

ssim = ssim(s, t, data_range=s.max() - s.min(), multichannel=True)

print('ssim: {}'.format(ssim))

all_pixels = Counter()
saturated_pixels = Counter()
unsaturated_pixels = Counter()


def is_saturated(r, g, b):
    Y = utils.get_brightness_max(r, g, b)
    return Y >= 1


for row in range(exr.get_height(s)):
    s_row = s[row]
    t_row = t[row]
    for column in range(exr.get_width(s)):
        s_pixel = s_row[column]
        t_pixel = t_row[column]

        all_pixels.process_pixel(s_pixel[0], s_pixel[1], s_pixel[2], t_pixel[0], t_pixel[1], t_pixel[2])
        if is_saturated(s_pixel[0], s_pixel[1], s_pixel[2]):
            saturated_pixels.process_pixel(s_pixel[0], s_pixel[1], s_pixel[2], t_pixel[0], t_pixel[1], t_pixel[2])
        else:
            unsaturated_pixels.process_pixel(s_pixel[0], s_pixel[1], s_pixel[2], t_pixel[0], t_pixel[1], t_pixel[2])


def print_results(counter, prefix = ''):
    print(prefix + 'pixel_count: {}'.format(counter.pixel_count))
    print(prefix + 'sqr_difference_total: {}'.format(counter.sqr_difference_total))
    print(prefix + 'avg sqrt(sqr_difference_total) per pixel: {0:.8f}'.format(counter.get_average_difference()))

print_results(all_pixels, '')
print_results(saturated_pixels, 'saturated_pixels ')
print_results(unsaturated_pixels, 'unsaturated_pixels ')

print('saturation: {0:.4f}'.format(float(saturated_pixels.pixel_count) / all_pixels.pixel_count))
