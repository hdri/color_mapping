def get_brightness_sRGB(R, G, B): # from linear space to linear space -> TODO: rename to get_luma (ITU BT.709)
    return 0.2126 * R + 0.7152 * G + 0.0722 * B

#def get_brightness_sRGB(R, G, B): # from linear space to linear space -> TODO: rename to get_luma (ITU BT.601)
#    return 0.299 * R + 0.587 * G + 0.114 * B

def get_brightness_max(R, G, B):
    return max(R, G, B)
