import exr
import sys
import math
import numpy as np
import cv2

if len(sys.argv) - 1 != 2:
    print("Usage: fix_mapping_HDRCNN.py in.exr out.exr")
    exit(1)

input_file = sys.argv[1]
output_file = sys.argv[2]

img = exr.open(input_file)


def transform(x):
    return x * 0.7 + math.pow(x * 0.45, 1.5)  # empirical straightening of HDRCNN's non-linearity
    # 1. run an image inference in HDRCNN
    #    $ python hdrcnn_predict.py --im_dir input_image.png --width 512 --height 384
    # 2. compute pixel value differences (prevent removal of the mapping csv at the end of the script)
    #    $ ./get_mapping_and_plot.sh input_image.png inference_output.exr
    # 3. ignore the points "below the curve" (use just one channel (red))
    #    $ python max_y_for_x.py mapping_[...].csv 0 1 > mapping_[...]_max.csv
    # 4. ignore the part of curve where is "breaks" (stops being smooth) (use just x < 0.75)
    # 5. find "a * x + (b * x)^c" coefficients via numerical optimization (minimize MSE)
    # (frame_0.111111_small_NN.png - flashky/frame_0.111111.png, downsampled to 512x384 using nearest neighbour)
    # ("mapping_png_vs_reconstructed.xlsx, Sheet4 (3)")


img = np.float32(img)
transform_vectorized = np.vectorize(transform)
img = transform_vectorized(img)

cv2.imwrite(output_file, cv2.cvtColor(np.float32(img), cv2.COLOR_RGB2BGR))
