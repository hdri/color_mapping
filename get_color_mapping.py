import sys
import exr
import utils

# Generate a csv containing brightness of pixels from two files (each line ~ 1 pixel, each column ~ one image)

# adapted from http://excamera.com/sphinx/articles-openexr.html

get_brightness = utils.get_brightness_max

def printUsageAndExit():
    print('usage: get_color_mapping.py <--luminance|--rgb> source.exr target.exr')
    sys.exit(1)

if len(sys.argv) != 3 + 1:
    printUsageAndExit()

if sys.argv[1] == "--luminance":
    rgb = False
elif sys.argv[1] == "--rgb":
    rgb = True
else:
    printUsageAndExit()

# Open the images
(s, t) = exr.open_two_images(sys.argv[2], sys.argv[3])

# Print the CSV header
if rgb:
    print('{},{},{},{},{},{}'.format('source_R', 'target_R', 'source_G', 'target_G', 'source_B', 'target_B'))
else:
    print('{},{}'.format('source_Y', 'target_Y')) # "original value, recovered value"


# Print value pair for each pixel
for row in range(exr.get_height(s)):
    s_row = s[row]
    t_row = t[row]
    for column in range(exr.get_width(s)):
        s_pixel = s_row[column]
        t_pixel = t_row[column]
        #print s_pixel
        #if sR[i] > 1:
        #    continue
        sY = get_brightness(s_pixel[0], s_pixel[1], s_pixel[2])
        tY = get_brightness(t_pixel[0], t_pixel[1], t_pixel[2])
        if rgb:
            print('{},{},{},{},{},{}'.format(s_pixel[0], t_pixel[0], s_pixel[1], t_pixel[1], s_pixel[2], t_pixel[2]))
        else:
            print('{},{}'.format(sY, tY))
            #print('%f,%f' % format(sR[i],tR[i]))
