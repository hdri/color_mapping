#!/bin/sh

# TODO: parameter count check

DIR="$(dirname "$(realpath "$0")")"

INPUT_CSV="$1"
OUTPUT_PNG=$(basename "$INPUT_CSV").png

MAXIMUM=$(python3 "$DIR"/get_maximum.py --ceil $INPUT_CSV 0 1)

echo "
#set term postscript enhanced color
#set output 'OUTPUTFILE.eps'
set terminal png truecolor size 2048,2048 enhanced font 'Verdana,25'

set xlabel 'ground truth luminance'
set ylabel 'reconstruction luminance'

set output '$OUTPUT_PNG'
#set terminal svg size 1024,1024 fname 'Verdana' fsize 10
#set output 'OUTPUTFILE.svg'
#set title 'OUTPUTFILE'
set style fill transparent solid 0.02 noborder
set style circle radius 0.005
#set xrange [0:10]
#set yrange [0:10]
#set xrange [0:*]
#set yrange [0:*]
set xrange [0:$MAXIMUM]
set yrange [0:$MAXIMUM]
#set view equal xy
#set size square
#set size ratio -1
#set logscale xy
set datafile separator ','
#plot '$INPUT_CSV' using 1:2 with circles lc rgb 'red',\
#	'$INPUT_CSV' using 3:4 with circles lc rgb 'green',\
#	'$INPUT_CSV' using 5:6 with circles lc rgb 'blue'
plot '$INPUT_CSV' using 1:2 with circles lc rgb 'black', x**1
" | gnuplot
