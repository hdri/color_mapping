import csv
import argparse
import math

parser = argparse.ArgumentParser(description='Find maximum value')
parser.add_argument('infile', nargs=1, help='input csv file')
parser.add_argument('columns', metavar='column', type=int, nargs='+', help='column index to check for maximum value')
parser.add_argument('--ceil', action='store_true', help='round up (ceiling)')

args = parser.parse_args()

#print(args.infile)
#print(args.columns)
#print(args.ceil)

columns = args.columns

maximum = float('-Inf')

with open(args.infile[0], 'r') as csvfile:
    reader = csv.reader(csvfile, delimiter=',', quotechar='"')
    next(reader)  # skip header
    for row in reader:
        for col in columns:
            val = float(row[col])
            if val > maximum:
                maximum = val
        #print(', '.join(row))

if args.ceil:
    maximum = math.ceil(maximum)

print(maximum)