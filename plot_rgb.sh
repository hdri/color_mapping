#!/bin/sh

DIR="$(dirname "$(realpath "$0")")"

# TODO: parameter count check

INPUT_CSV="$1"
OUTPUT="$(basename "$INPUT_CSV")"
OUTPUT_PNG="$OUTPUT.png"

MAXIMUM=$(python3 "$DIR"/get_maximum.py --ceil $INPUT_CSV 0 1 2 3 4 5)

generate_channel() {
	# "red", "green" or "blue"
	CHANNEL="$1"
	COLUMN_SOURCE="$2"
	COLUMN_TARGET="$3"

	echo "
#set term postscript enhanced color
#set output 'OUTPUTFILE.eps'
#set terminal png truecolor transparent size 2048,2048 enhanced font 'Verdana,10'
set terminal png truecolor background rgb 'black' size 2048,2048 enhanced font 'Verdana,25'

set xlabel 'ground truth color value' tc rgb 'white'
set ylabel 'reconstruction color value' tc rgb 'white'
set border lc rgb 'white'
set key tc rgb 'white'
set key off # disable legend

set output '$OUTPUT.$CHANNEL.png'
#set terminal svg size 1024,1024 fname 'Verdana' fsize 10
#set output 'OUTPUTFILE.svg'
#set title 'OUTPUTFILE'
set style fill transparent solid 0.02 noborder
set style circle radius 0.005
set xrange [0:$MAXIMUM]
set yrange [0:$MAXIMUM]
set datafile separator ','
plot '$INPUT_CSV' using $COLUMN_SOURCE:$COLUMN_TARGET with circles lc rgb '$CHANNEL'
#plot '$INPUT_CSV' using 1:2 with circles lc rgb 'red',\
#    '$INPUT_CSV' using 3:4 with circles lc rgb 'green',\
#    '$INPUT_CSV' using 5:6 with circles lc rgb 'blue'
" | gnuplot

}

generate_channel red 1 2
generate_channel green 3 4
generate_channel blue 5 6

composite -compose add  "$OUTPUT.red.png" "$OUTPUT.green.png"  "$OUTPUT.RG.png"
composite -compose add  "$OUTPUT.RG.png" "$OUTPUT.blue.png"  "$OUTPUT_PNG"

rm "$OUTPUT.red.png" "$OUTPUT.green.png" "$OUTPUT.RG.png" "$OUTPUT.blue.png"
