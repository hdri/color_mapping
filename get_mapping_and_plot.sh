#!/bin/sh

DIR="$(dirname "$(realpath "$0")")"

set -ex

# TODO: check input files, parameter count

SOURCE="$1"
TARGET="$2"

SOURCE_BASENAME=$(basename "$SOURCE")
TARGET_BASENAME=$(basename "$TARGET")

MAPPING_CSV_NAME="mapping_${SOURCE_BASENAME}_${TARGET_BASENAME}.csv"
RGB_MAPPING_CSV_NAME="mapping_${SOURCE_BASENAME}_${TARGET_BASENAME}.RGB.csv"

python2 "$DIR"/get_color_mapping.py --luminance "$SOURCE" "$TARGET" > $MAPPING_CSV_NAME
python2 "$DIR"/get_color_mapping.py --rgb "$SOURCE" "$TARGET" > $RGB_MAPPING_CSV_NAME

# Plot luminance and RGB mapping
"$DIR"/plot.sh $MAPPING_CSV_NAME
"$DIR"/plot_rgb.sh $RGB_MAPPING_CSV_NAME

# cleanup
rm $MAPPING_CSV_NAME
rm $RGB_MAPPING_CSV_NAME

