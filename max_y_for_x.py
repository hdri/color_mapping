import csv
import argparse

parser = argparse.ArgumentParser(description='Find maximum value')
parser.add_argument('infile', nargs=1, help='input csv file')
parser.add_argument('x_column', metavar='column_x', type=int, help='column x index')
parser.add_argument('columns', metavar='column', type=int, nargs='+', help='column index (y values) to check for maximum value')

args = parser.parse_args()

#print(args.infile)
#print(args.columns)
#print(args.ceil)

columns = args.columns
x_column = args.x_column

maximum = {}

with open(args.infile[0], 'r') as csvfile:
    reader = csv.reader(csvfile, delimiter=',', quotechar='"')
    next(reader)  # skip header
    for row in reader:
        x = row[x_column]
        for col in columns:
            val = float(row[col])
            old_maximum = maximum.get(x, float('-Inf'))
            if val > old_maximum:
                maximum[x] = val
        #print(', '.join(row))

for x, y in maximum.items():
    print("{},{}".format(x, y))
